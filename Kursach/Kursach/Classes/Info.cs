﻿using System;
public class Info
{
    public int Id { get; set; }
    public string Name { get; set; }

    public override string ToString()
    {
        return Name;
    }

    public Info(int i, string n)
    {
        Id = i;
        Name = n;
    }
}
