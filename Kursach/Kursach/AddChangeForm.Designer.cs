﻿
namespace Kursach
{
    partial class AddChangeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Страны = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.СтранаИмя = new System.Windows.Forms.TextBox();
            this.Заказчики = new System.Windows.Forms.GroupBox();
            this.ЗаказчикСтрана = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ЗаказчикИмя = new System.Windows.Forms.TextBox();
            this.Проекты = new System.Windows.Forms.GroupBox();
            this.ПроектЗаказчик = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ПроектКонец = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.ПроектНачало = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ПроектБюджет = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Рольвпроекте = new System.Windows.Forms.GroupBox();
            this.РольПроект = new System.Windows.Forms.ComboBox();
            this.РольСотрудник = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.РольНазвание = new System.Windows.Forms.TextBox();
            this.Стажирующиеся = new System.Windows.Forms.GroupBox();
            this.СтажёрКуратор = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.СтажёрФИО = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.Стажировка = new System.Windows.Forms.GroupBox();
            this.СтажировкаКурс = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.СтажировкаДатаТеста = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.СтажировкаТест = new System.Windows.Forms.TextBox();
            this.СтажировкаСтажёр = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Курсыприкомпании = new System.Windows.Forms.GroupBox();
            this.КурсДатаКонца = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.КурсДатаНачала = new System.Windows.Forms.DateTimePicker();
            this.label29 = new System.Windows.Forms.Label();
            this.КурсНазвание = new System.Windows.Forms.TextBox();
            this.КурсТехнология = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Технологиивпроекте = new System.Windows.Forms.GroupBox();
            this.ТехнПроектПроект = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.ТехнПроектаВид = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.Видытехнологий = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.ТехнологияНазвание = new System.Windows.Forms.TextBox();
            this.Сотрудники = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.СотрудникФИО = new System.Windows.Forms.TextBox();
            this.СотрудникУвол = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.СотрудникПриём = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.СотрудникТелефон = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.ПроектТип = new System.Windows.Forms.ComboBox();
            this.Страны.SuspendLayout();
            this.Заказчики.SuspendLayout();
            this.Проекты.SuspendLayout();
            this.Рольвпроекте.SuspendLayout();
            this.Стажирующиеся.SuspendLayout();
            this.Стажировка.SuspendLayout();
            this.Курсыприкомпании.SuspendLayout();
            this.Технологиивпроекте.SuspendLayout();
            this.Видытехнологий.SuspendLayout();
            this.Сотрудники.SuspendLayout();
            this.SuspendLayout();
            // 
            // Страны
            // 
            this.Страны.Controls.Add(this.label1);
            this.Страны.Controls.Add(this.СтранаИмя);
            this.Страны.Location = new System.Drawing.Point(12, 12);
            this.Страны.Name = "Страны";
            this.Страны.Size = new System.Drawing.Size(250, 50);
            this.Страны.TabIndex = 0;
            this.Страны.TabStop = false;
            this.Страны.Text = "Страны";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Название";
            // 
            // СтранаИмя
            // 
            this.СтранаИмя.Location = new System.Drawing.Point(100, 19);
            this.СтранаИмя.Name = "СтранаИмя";
            this.СтранаИмя.Size = new System.Drawing.Size(144, 20);
            this.СтранаИмя.TabIndex = 0;
            // 
            // Заказчики
            // 
            this.Заказчики.Controls.Add(this.ЗаказчикСтрана);
            this.Заказчики.Controls.Add(this.label3);
            this.Заказчики.Controls.Add(this.label2);
            this.Заказчики.Controls.Add(this.ЗаказчикИмя);
            this.Заказчики.Location = new System.Drawing.Point(12, 68);
            this.Заказчики.Name = "Заказчики";
            this.Заказчики.Size = new System.Drawing.Size(250, 76);
            this.Заказчики.TabIndex = 2;
            this.Заказчики.TabStop = false;
            this.Заказчики.Text = "Заказчики";
            // 
            // ЗаказчикСтрана
            // 
            this.ЗаказчикСтрана.FormattingEnabled = true;
            this.ЗаказчикСтрана.Location = new System.Drawing.Point(100, 45);
            this.ЗаказчикСтрана.Name = "ЗаказчикСтрана";
            this.ЗаказчикСтрана.Size = new System.Drawing.Size(144, 21);
            this.ЗаказчикСтрана.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Страна";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Наименование";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // ЗаказчикИмя
            // 
            this.ЗаказчикИмя.Location = new System.Drawing.Point(100, 19);
            this.ЗаказчикИмя.Name = "ЗаказчикИмя";
            this.ЗаказчикИмя.Size = new System.Drawing.Size(144, 20);
            this.ЗаказчикИмя.TabIndex = 0;
            // 
            // Проекты
            // 
            this.Проекты.Controls.Add(this.ПроектТип);
            this.Проекты.Controls.Add(this.ПроектЗаказчик);
            this.Проекты.Controls.Add(this.label10);
            this.Проекты.Controls.Add(this.ПроектКонец);
            this.Проекты.Controls.Add(this.label9);
            this.Проекты.Controls.Add(this.ПроектНачало);
            this.Проекты.Controls.Add(this.label8);
            this.Проекты.Controls.Add(this.label4);
            this.Проекты.Controls.Add(this.ПроектБюджет);
            this.Проекты.Controls.Add(this.label5);
            this.Проекты.Location = new System.Drawing.Point(12, 150);
            this.Проекты.Name = "Проекты";
            this.Проекты.Size = new System.Drawing.Size(250, 154);
            this.Проекты.TabIndex = 4;
            this.Проекты.TabStop = false;
            this.Проекты.Text = "Проекты";
            // 
            // ПроектЗаказчик
            // 
            this.ПроектЗаказчик.FormattingEnabled = true;
            this.ПроектЗаказчик.Location = new System.Drawing.Point(100, 122);
            this.ПроектЗаказчик.Name = "ПроектЗаказчик";
            this.ПроектЗаказчик.Size = new System.Drawing.Size(144, 21);
            this.ПроектЗаказчик.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Заказчик";
            // 
            // ПроектКонец
            // 
            this.ПроектКонец.Location = new System.Drawing.Point(100, 97);
            this.ПроектКонец.Name = "ПроектКонец";
            this.ПроектКонец.Size = new System.Drawing.Size(144, 20);
            this.ПроектКонец.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Дата окончания";
            // 
            // ПроектНачало
            // 
            this.ПроектНачало.Location = new System.Drawing.Point(100, 71);
            this.ПроектНачало.Name = "ПроектНачало";
            this.ПроектНачало.Size = new System.Drawing.Size(144, 20);
            this.ПроектНачало.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Дата начала";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Бюджет";
            // 
            // ПроектБюджет
            // 
            this.ПроектБюджет.Location = new System.Drawing.Point(100, 45);
            this.ПроектБюджет.Name = "ПроектБюджет";
            this.ПроектБюджет.Size = new System.Drawing.Size(144, 20);
            this.ПроектБюджет.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Тип";
            // 
            // Рольвпроекте
            // 
            this.Рольвпроекте.Controls.Add(this.РольПроект);
            this.Рольвпроекте.Controls.Add(this.РольСотрудник);
            this.Рольвпроекте.Controls.Add(this.label7);
            this.Рольвпроекте.Controls.Add(this.label12);
            this.Рольвпроекте.Controls.Add(this.label13);
            this.Рольвпроекте.Controls.Add(this.РольНазвание);
            this.Рольвпроекте.Location = new System.Drawing.Point(268, 12);
            this.Рольвпроекте.Name = "Рольвпроекте";
            this.Рольвпроекте.Size = new System.Drawing.Size(250, 105);
            this.Рольвпроекте.TabIndex = 10;
            this.Рольвпроекте.TabStop = false;
            this.Рольвпроекте.Text = "Роль в проекте";
            // 
            // РольПроект
            // 
            this.РольПроект.DropDownWidth = 450;
            this.РольПроект.FormattingEnabled = true;
            this.РольПроект.Location = new System.Drawing.Point(100, 72);
            this.РольПроект.Name = "РольПроект";
            this.РольПроект.Size = new System.Drawing.Size(144, 21);
            this.РольПроект.TabIndex = 12;
            // 
            // РольСотрудник
            // 
            this.РольСотрудник.DropDownWidth = 250;
            this.РольСотрудник.FormattingEnabled = true;
            this.РольСотрудник.Location = new System.Drawing.Point(100, 45);
            this.РольСотрудник.Name = "РольСотрудник";
            this.РольСотрудник.Size = new System.Drawing.Size(144, 21);
            this.РольСотрудник.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Проект";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Сотрудник";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Наименование";
            // 
            // РольНазвание
            // 
            this.РольНазвание.Location = new System.Drawing.Point(100, 19);
            this.РольНазвание.Name = "РольНазвание";
            this.РольНазвание.Size = new System.Drawing.Size(144, 20);
            this.РольНазвание.TabIndex = 0;
            // 
            // Стажирующиеся
            // 
            this.Стажирующиеся.Controls.Add(this.СтажёрКуратор);
            this.Стажирующиеся.Controls.Add(this.label24);
            this.Стажирующиеся.Controls.Add(this.СтажёрФИО);
            this.Стажирующиеся.Controls.Add(this.label23);
            this.Стажирующиеся.Location = new System.Drawing.Point(524, 12);
            this.Стажирующиеся.Name = "Стажирующиеся";
            this.Стажирующиеся.Size = new System.Drawing.Size(250, 77);
            this.Стажирующиеся.TabIndex = 17;
            this.Стажирующиеся.TabStop = false;
            this.Стажирующиеся.Text = "Стажирующиеся";
            // 
            // СтажёрКуратор
            // 
            this.СтажёрКуратор.DropDownWidth = 250;
            this.СтажёрКуратор.FormattingEnabled = true;
            this.СтажёрКуратор.Location = new System.Drawing.Point(100, 44);
            this.СтажёрКуратор.Name = "СтажёрКуратор";
            this.СтажёрКуратор.Size = new System.Drawing.Size(144, 21);
            this.СтажёрКуратор.TabIndex = 19;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(37, 47);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Куратор";
            // 
            // СтажёрФИО
            // 
            this.СтажёрФИО.Location = new System.Drawing.Point(100, 18);
            this.СтажёрФИО.Name = "СтажёрФИО";
            this.СтажёрФИО.Size = new System.Drawing.Size(144, 20);
            this.СтажёрФИО.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(37, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "ФИО";
            // 
            // Стажировка
            // 
            this.Стажировка.Controls.Add(this.СтажировкаКурс);
            this.Стажировка.Controls.Add(this.label21);
            this.Стажировка.Controls.Add(this.СтажировкаДатаТеста);
            this.Стажировка.Controls.Add(this.label20);
            this.Стажировка.Controls.Add(this.СтажировкаТест);
            this.Стажировка.Controls.Add(this.СтажировкаСтажёр);
            this.Стажировка.Controls.Add(this.label18);
            this.Стажировка.Controls.Add(this.label19);
            this.Стажировка.Location = new System.Drawing.Point(524, 123);
            this.Стажировка.Name = "Стажировка";
            this.Стажировка.Size = new System.Drawing.Size(250, 129);
            this.Стажировка.TabIndex = 20;
            this.Стажировка.TabStop = false;
            this.Стажировка.Text = "Стажировка";
            // 
            // СтажировкаКурс
            // 
            this.СтажировкаКурс.DropDownWidth = 200;
            this.СтажировкаКурс.FormattingEnabled = true;
            this.СтажировкаКурс.Location = new System.Drawing.Point(100, 100);
            this.СтажировкаКурс.Name = "СтажировкаКурс";
            this.СтажировкаКурс.Size = new System.Drawing.Size(144, 21);
            this.СтажировкаКурс.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(37, 105);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "Курс";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // СтажировкаДатаТеста
            // 
            this.СтажировкаДатаТеста.Location = new System.Drawing.Point(100, 74);
            this.СтажировкаДатаТеста.Name = "СтажировкаДатаТеста";
            this.СтажировкаДатаТеста.Size = new System.Drawing.Size(144, 20);
            this.СтажировкаДатаТеста.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(21, 79);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "Дата теста";
            // 
            // СтажировкаТест
            // 
            this.СтажировкаТест.Location = new System.Drawing.Point(100, 47);
            this.СтажировкаТест.Name = "СтажировкаТест";
            this.СтажировкаТест.Size = new System.Drawing.Size(144, 20);
            this.СтажировкаТест.TabIndex = 20;
            // 
            // СтажировкаСтажёр
            // 
            this.СтажировкаСтажёр.FormattingEnabled = true;
            this.СтажировкаСтажёр.Location = new System.Drawing.Point(100, 15);
            this.СтажировкаСтажёр.Name = "СтажировкаСтажёр";
            this.СтажировкаСтажёр.Size = new System.Drawing.Size(144, 21);
            this.СтажировкаСтажёр.TabIndex = 20;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Результат теста";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "ФИО стажёра";
            // 
            // Курсыприкомпании
            // 
            this.Курсыприкомпании.Controls.Add(this.КурсДатаКонца);
            this.Курсыприкомпании.Controls.Add(this.label30);
            this.Курсыприкомпании.Controls.Add(this.КурсДатаНачала);
            this.Курсыприкомпании.Controls.Add(this.label29);
            this.Курсыприкомпании.Controls.Add(this.КурсНазвание);
            this.Курсыприкомпании.Controls.Add(this.КурсТехнология);
            this.Курсыприкомпании.Controls.Add(this.label27);
            this.Курсыприкомпании.Controls.Add(this.label31);
            this.Курсыприкомпании.Location = new System.Drawing.Point(524, 310);
            this.Курсыприкомпании.Name = "Курсыприкомпании";
            this.Курсыприкомпании.Size = new System.Drawing.Size(250, 128);
            this.Курсыприкомпании.TabIndex = 26;
            this.Курсыприкомпании.TabStop = false;
            this.Курсыприкомпании.Text = "Курсы при компании";
            // 
            // КурсДатаКонца
            // 
            this.КурсДатаКонца.Location = new System.Drawing.Point(100, 69);
            this.КурсДатаКонца.Name = "КурсДатаКонца";
            this.КурсДатаКонца.Size = new System.Drawing.Size(144, 20);
            this.КурсДатаКонца.TabIndex = 29;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(11, 75);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(89, 13);
            this.label30.TabIndex = 30;
            this.label30.Text = "Дата окончания";
            // 
            // КурсДатаНачала
            // 
            this.КурсДатаНачала.Location = new System.Drawing.Point(100, 43);
            this.КурсДатаНачала.Name = "КурсДатаНачала";
            this.КурсДатаНачала.Size = new System.Drawing.Size(144, 20);
            this.КурсДатаНачала.TabIndex = 27;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(21, 48);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 13);
            this.label29.TabIndex = 28;
            this.label29.Text = "Дата начала";
            // 
            // КурсНазвание
            // 
            this.КурсНазвание.Location = new System.Drawing.Point(100, 17);
            this.КурсНазвание.Name = "КурсНазвание";
            this.КурсНазвание.Size = new System.Drawing.Size(144, 20);
            this.КурсНазвание.TabIndex = 26;
            // 
            // КурсТехнология
            // 
            this.КурсТехнология.FormattingEnabled = true;
            this.КурсТехнология.Location = new System.Drawing.Point(100, 96);
            this.КурсТехнология.Name = "КурсТехнология";
            this.КурсТехнология.Size = new System.Drawing.Size(144, 21);
            this.КурсТехнология.TabIndex = 23;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(19, 99);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(66, 13);
            this.label27.TabIndex = 22;
            this.label27.Text = "Технология";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 19);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 13);
            this.label31.TabIndex = 3;
            this.label31.Text = "Название курса";
            // 
            // Технологиивпроекте
            // 
            this.Технологиивпроекте.Controls.Add(this.ТехнПроектПроект);
            this.Технологиивпроекте.Controls.Add(this.label36);
            this.Технологиивпроекте.Controls.Add(this.ТехнПроектаВид);
            this.Технологиивпроекте.Controls.Add(this.label35);
            this.Технологиивпроекте.Location = new System.Drawing.Point(268, 123);
            this.Технологиивпроекте.Name = "Технологиивпроекте";
            this.Технологиивпроекте.Size = new System.Drawing.Size(250, 73);
            this.Технологиивпроекте.TabIndex = 10;
            this.Технологиивпроекте.TabStop = false;
            this.Технологиивпроекте.Text = "Технологии в проекте";
            this.Технологиивпроекте.Enter += new System.EventHandler(this.groupBox8_Enter);
            // 
            // ТехнПроектПроект
            // 
            this.ТехнПроектПроект.DropDownWidth = 450;
            this.ТехнПроектПроект.FormattingEnabled = true;
            this.ТехнПроектПроект.Location = new System.Drawing.Point(100, 44);
            this.ТехнПроектПроект.Name = "ТехнПроектПроект";
            this.ТехнПроектПроект.Size = new System.Drawing.Size(144, 21);
            this.ТехнПроектПроект.TabIndex = 14;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(37, 49);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 13);
            this.label36.TabIndex = 12;
            this.label36.Text = "Проект";
            // 
            // ТехнПроектаВид
            // 
            this.ТехнПроектаВид.FormattingEnabled = true;
            this.ТехнПроектаВид.Location = new System.Drawing.Point(100, 18);
            this.ТехнПроектаВид.Name = "ТехнПроектаВид";
            this.ТехнПроектаВид.Size = new System.Drawing.Size(144, 21);
            this.ТехнПроектаВид.TabIndex = 10;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(18, 22);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(66, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Технология";
            // 
            // Видытехнологий
            // 
            this.Видытехнологий.Controls.Add(this.label37);
            this.Видытехнологий.Controls.Add(this.ТехнологияНазвание);
            this.Видытехнологий.Location = new System.Drawing.Point(268, 202);
            this.Видытехнологий.Name = "Видытехнологий";
            this.Видытехнологий.Size = new System.Drawing.Size(250, 50);
            this.Видытехнологий.TabIndex = 2;
            this.Видытехнологий.TabStop = false;
            this.Видытехнологий.Text = "Виды технологий";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 22);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(57, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Название";
            // 
            // ТехнологияНазвание
            // 
            this.ТехнологияНазвание.Location = new System.Drawing.Point(100, 19);
            this.ТехнологияНазвание.Name = "ТехнологияНазвание";
            this.ТехнологияНазвание.Size = new System.Drawing.Size(144, 20);
            this.ТехнологияНазвание.TabIndex = 0;
            // 
            // Сотрудники
            // 
            this.Сотрудники.Controls.Add(this.label41);
            this.Сотрудники.Controls.Add(this.СотрудникФИО);
            this.Сотрудники.Controls.Add(this.СотрудникУвол);
            this.Сотрудники.Controls.Add(this.label40);
            this.Сотрудники.Controls.Add(this.СотрудникПриём);
            this.Сотрудники.Controls.Add(this.label39);
            this.Сотрудники.Controls.Add(this.label38);
            this.Сотрудники.Controls.Add(this.СотрудникТелефон);
            this.Сотрудники.Location = new System.Drawing.Point(12, 310);
            this.Сотрудники.Name = "Сотрудники";
            this.Сотрудники.Size = new System.Drawing.Size(250, 155);
            this.Сотрудники.TabIndex = 3;
            this.Сотрудники.TabStop = false;
            this.Сотрудники.Text = "Сотрудники";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(29, 96);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(34, 13);
            this.label41.TabIndex = 15;
            this.label41.Text = "ФИО";
            this.label41.Click += new System.EventHandler(this.label41_Click);
            // 
            // СотрудникФИО
            // 
            this.СотрудникФИО.Location = new System.Drawing.Point(100, 95);
            this.СотрудникФИО.Name = "СотрудникФИО";
            this.СотрудникФИО.Size = new System.Drawing.Size(144, 20);
            this.СотрудникФИО.TabIndex = 14;
            // 
            // СотрудникУвол
            // 
            this.СотрудникУвол.Location = new System.Drawing.Point(100, 69);
            this.СотрудникУвол.Name = "СотрудникУвол";
            this.СотрудникУвол.Size = new System.Drawing.Size(144, 20);
            this.СотрудникУвол.TabIndex = 13;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 72);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(95, 13);
            this.label40.TabIndex = 12;
            this.label40.Text = "Дата увольнения";
            // 
            // СотрудникПриём
            // 
            this.СотрудникПриём.Location = new System.Drawing.Point(100, 44);
            this.СотрудникПриём.Name = "СотрудникПриём";
            this.СотрудникПриём.Size = new System.Drawing.Size(144, 20);
            this.СотрудникПриём.TabIndex = 11;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(15, 44);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(74, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "Дата приёма";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(20, 20);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(52, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Телефон";
            // 
            // СотрудникТелефон
            // 
            this.СотрудникТелефон.Location = new System.Drawing.Point(100, 19);
            this.СотрудникТелефон.Name = "СотрудникТелефон";
            this.СотрудникТелефон.Size = new System.Drawing.Size(144, 20);
            this.СотрудникТелефон.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAdd.Location = new System.Drawing.Point(780, 23);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(216, 39);
            this.btnAdd.TabIndex = 27;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.Button_Click);
            // 
            // ПроектТип
            // 
            this.ПроектТип.FormattingEnabled = true;
            this.ПроектТип.Items.AddRange(new object[] {
            "Внутренний",
            "Внешний"});
            this.ПроектТип.Location = new System.Drawing.Point(100, 17);
            this.ПроектТип.Name = "ПроектТип";
            this.ПроектТип.Size = new System.Drawing.Size(144, 21);
            this.ПроектТип.TabIndex = 28;
            // 
            // AddChangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 473);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.Сотрудники);
            this.Controls.Add(this.Видытехнологий);
            this.Controls.Add(this.Технологиивпроекте);
            this.Controls.Add(this.Курсыприкомпании);
            this.Controls.Add(this.Стажировка);
            this.Controls.Add(this.Стажирующиеся);
            this.Controls.Add(this.Рольвпроекте);
            this.Controls.Add(this.Проекты);
            this.Controls.Add(this.Заказчики);
            this.Controls.Add(this.Страны);
            this.Name = "AddChangeForm";
            this.Text = "Form2";
            this.Страны.ResumeLayout(false);
            this.Страны.PerformLayout();
            this.Заказчики.ResumeLayout(false);
            this.Заказчики.PerformLayout();
            this.Проекты.ResumeLayout(false);
            this.Проекты.PerformLayout();
            this.Рольвпроекте.ResumeLayout(false);
            this.Рольвпроекте.PerformLayout();
            this.Стажирующиеся.ResumeLayout(false);
            this.Стажирующиеся.PerformLayout();
            this.Стажировка.ResumeLayout(false);
            this.Стажировка.PerformLayout();
            this.Курсыприкомпании.ResumeLayout(false);
            this.Курсыприкомпании.PerformLayout();
            this.Технологиивпроекте.ResumeLayout(false);
            this.Технологиивпроекте.PerformLayout();
            this.Видытехнологий.ResumeLayout(false);
            this.Видытехнологий.PerformLayout();
            this.Сотрудники.ResumeLayout(false);
            this.Сотрудники.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Страны;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox СтранаИмя;
        private System.Windows.Forms.GroupBox Заказчики;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ЗаказчикИмя;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox Проекты;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ПроектБюджет;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ЗаказчикСтрана;
        private System.Windows.Forms.DateTimePicker ПроектКонец;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker ПроектНачало;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox ПроектЗаказчик;
        private System.Windows.Forms.GroupBox Рольвпроекте;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox РольНазвание;
        private System.Windows.Forms.ComboBox РольПроект;
        private System.Windows.Forms.ComboBox РольСотрудник;
        private System.Windows.Forms.GroupBox Стажирующиеся;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox СтажёрКуратор;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox СтажёрФИО;
        private System.Windows.Forms.GroupBox Стажировка;
        private System.Windows.Forms.ComboBox СтажировкаКурс;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker СтажировкаДатаТеста;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox СтажировкаТест;
        private System.Windows.Forms.ComboBox СтажировкаСтажёр;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox Курсыприкомпании;
        private System.Windows.Forms.ComboBox КурсТехнология;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker КурсДатаКонца;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DateTimePicker КурсДатаНачала;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox КурсНазвание;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox Технологиивпроекте;
        private System.Windows.Forms.ComboBox ТехнПроектаВид;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox ТехнПроектПроект;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox Видытехнологий;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox ТехнологияНазвание;
        private System.Windows.Forms.GroupBox Сотрудники;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox СотрудникФИО;
        private System.Windows.Forms.DateTimePicker СотрудникУвол;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.DateTimePicker СотрудникПриём;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox СотрудникТелефон;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox ПроектТип;
    }
}