﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach
{
    public partial class AddChangeForm : Form
    {
        //Форма добавления/изменения записей таблицы

        DataSet ds;
        SqlDataAdapter adapter;
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        string st = "";

        GroupBox grx;
        int DataId;

        public AddChangeForm()
        {
            InitializeComponent();
            ПроектТип.SelectedIndex = 0;
        }

        private void formStart(string str)
        {
            InitializeComponent();
            HideAll();
            ClearAll();
            grx = this.Controls.Find(str, true).FirstOrDefault() as GroupBox;
            grx.Visible = true;
            grx.Location = new Point(100, 35);
            this.Height = grx.Height + 200 + btnAdd.Height;
            this.Width = grx.Width + 200;
            btnAdd.Location = new Point((this.Width - btnAdd.Width) / 2, grx.Height + 50);
        }

        public AddChangeForm(string str)
        {
            this.Text = str;
            formStart(String.Join("", str.Split(new char[] { ' ' })));
            grx.Text = "Добавление данных в таблицу";
            btnAdd.Text = "Добавить";
            st = str + "Add";

            switch (str)
            {
                case "Заказчики":
                    ЗаказчикСтрана.DataSource = DataS("Код, Название", "Страны");
                    ЗаказчикСтрана.DisplayMember = "Name";
                    ЗаказчикСтрана.ValueMember = "Id";
                    ЗаказчикСтрана.SelectedIndex = 0;
                    break;
                case "Проекты":
                    ПроектЗаказчик.DataSource = DataS("Код, Наименование", "Заказчики");
                    ПроектЗаказчик.DisplayMember = "Name";
                    ПроектЗаказчик.ValueMember = "Id";
                    ПроектЗаказчик.SelectedIndex = 0;
                    break;
                case "Роль в проекте":
                    РольСотрудник.DataSource = DataS("Код, CONCAT(ФИО, ' ', Телефон)", "Сотрудники");
                    РольСотрудник.DisplayMember = "Name";
                    РольСотрудник.ValueMember = "Id";
                    РольСотрудник.SelectedIndex = 0;
                    РольПроект.DataSource = DataS("Проекты.Код, CONCAT(Тип, ' проект с бюджетом ', Бюджет, ' ', [Дата начала], '-', [Дата окончания], '. ', Заказчики.Наименование)", "Проекты LEFT JOIN Заказчики ON Заказчики.Код = [Код заказчика]");
                    РольПроект.DisplayMember = "Name";
                    РольПроект.ValueMember = "Id";
                    РольПроект.SelectedIndex = 0;
                    break;
                case "Технологии в проекте":
                    ТехнПроектаВид.DataSource = DataS("Код, Название", "[Виды технологий]");
                    ТехнПроектаВид.DisplayMember = "Name";
                    ТехнПроектаВид.ValueMember = "Id";
                    ТехнПроектаВид.SelectedIndex = 0;

                    ТехнПроектПроект.DataSource = DataS("Проекты.Код, CONCAT(Тип, ' проект с бюджетом ', Бюджет, ' ', [Дата начала], '-', [Дата окончания], '. ', Заказчики.Наименование)", "Проекты LEFT JOIN Заказчики ON Заказчики.Код = [Код заказчика]");
                    ТехнПроектПроект.DisplayMember = "Name";
                    ТехнПроектПроект.ValueMember = "Id";
                    ТехнПроектПроект.SelectedIndex = 0;
                    break;
                case "Стажирующиеся":
                    СтажёрКуратор.DataSource = DataS("Код, CONCAT(ФИО, ' ', Телефон)", "Сотрудники");
                    СтажёрКуратор.DisplayMember = "Name";
                    СтажёрКуратор.ValueMember = "Id";
                    СтажёрКуратор.SelectedIndex = 0;
                    break;
                case "Стажировка":
                    СтажировкаСтажёр.DataSource = DataS("Код, ФИО", "Стажирующиеся");
                    СтажировкаСтажёр.DisplayMember = "Name";
                    СтажировкаСтажёр.ValueMember = "Id";
                    СтажировкаСтажёр.SelectedIndex = 0;

                    /*СтажировкаКуратор.DataSource = DataS("Код, CONCAT(ФИО, ' ', Телефон)", "Сотрудники");
                    СтажировкаКуратор.DisplayMember = "Name";
                    СтажировкаКуратор.ValueMember = "Id";
                    СтажировкаКуратор.SelectedIndex = 0;*/

                    СтажировкаКурс.DataSource = DataS("[Курсы при компании].Код, CONCAT([Курсы при компании].Название, '(', [Виды технологий].Название, ')')", "[Курсы при компании] INNER JOIN [Виды технологий] ON [Код технологии] = [Виды технологий].Код");
                    СтажировкаКурс.DisplayMember = "Name";
                    СтажировкаКурс.ValueMember = "Id";
                    СтажировкаКурс.SelectedIndex = 0;
                    break;
                case "Курсы при компании":
                    КурсТехнология.DataSource = DataS("Код, Название", "[Виды технологий]");
                    КурсТехнология.DisplayMember = "Name";
                    КурсТехнология.ValueMember = "Id";
                    КурсТехнология.SelectedIndex = 0;
                    break;
                default:
                    break;
            }
        }
        public AddChangeForm(string str, DataGridViewRow d)
        {
            formStart(String.Join("", str.Split(new char[] { ' ' })));
            grx.Text = "Изменение данных в таблице";
            btnAdd.Text = "Изменить";
            st = str + "Change";

            DataId = Convert.ToInt32(d.Cells[0].Value);
            switch (str)
            {
                case "Страны":
                    ShowCountries(d);
                    break;
                case "Заказчики":
                    ShowCustomers(d);
                    break;
                case "Проекты":
                    ShowProjects(d);
                    break;
                case "Сотрудники":
                    ShowEmployees(d);
                    break;
                case "Роль в проекте":
                    ShowRoles(d);
                    break;
                case "Технологии в проекте":
                    ShowTechsProj(d);
                    break;
                case "Виды технологий":
                    ShowTechs(d);
                    break;
                case "Стажирующиеся":
                    ShowTraineers(d);
                    break;
                case "Стажировка":
                    ShowTrainig(d);
                    break;
                case "Курсы при компании":
                    ShowCourses(d);
                    break;
                default:
                    break;
            }
        }

        public void ShowCountries(DataGridViewRow d)
        {
            СтранаИмя.Text = d.Cells[1].Value.ToString();
        }

        public void ShowCustomers(DataGridViewRow d)
        {
            ЗаказчикИмя.Text = d.Cells[1].Value.ToString();
            ЗаказчикСтрана.DataSource = DataS("Код, Название", "Страны");
            ЗаказчикСтрана.DisplayMember = "Name";
            ЗаказчикСтрана.ValueMember = "Id";
            ЗаказчикСтрана.SelectedIndex = ЗаказчикСтрана.FindStringExact(d.Cells[2].Value.ToString());
        }

        public void ShowProjects(DataGridViewRow d)
        {
            ПроектТип.Text = d.Cells[1].Value.ToString();
            ПроектБюджет.Text = d.Cells[2].Value.ToString().Replace(",", ".");
            ПроектНачало.Text = d.Cells[3].Value.ToString();
            ПроектКонец.Text = d.Cells[4].Value.ToString();
            ПроектЗаказчик.DataSource = DataS("Код, Наименование", "Заказчики");
            ПроектЗаказчик.DisplayMember = "Name";
            ПроектЗаказчик.ValueMember = "Id";
            ПроектЗаказчик.SelectedIndex = ПроектЗаказчик.FindStringExact(d.Cells[5].Value.ToString());
        }
        public void ShowEmployees(DataGridViewRow d)
        {
            СотрудникТелефон.Text = d.Cells[1].Value.ToString();
            СотрудникПриём.Text = d.Cells[2].Value.ToString();
            СотрудникУвол.Text = d.Cells[3].Value.ToString();
            СотрудникФИО.Text = d.Cells[4].Value.ToString();
        }
        public void ShowRoles(DataGridViewRow d)
        {
            РольНазвание.Text = d.Cells[1].Value.ToString();
            РольСотрудник.DataSource = DataS("Код, CONCAT(ФИО, ' ', Телефон)", "Сотрудники");
            РольСотрудник.DisplayMember = "Name";
            РольСотрудник.ValueMember = "Id";
            РольСотрудник.SelectedIndex = РольСотрудник.FindStringExact(d.Cells[2].Value.ToString() + " " + d.Cells[3].Value.ToString());
            РольПроект.DataSource = DataS("Проекты.Код, CONCAT(Тип, ' проект с бюджетом ', Бюджет, ' ', [Дата начала], '-', [Дата окончания], '. ', Заказчики.Наименование)", "Проекты LEFT JOIN Заказчики ON Заказчики.Код = [Код заказчика]");
            РольПроект.DisplayMember = "Name";
            РольПроект.ValueMember = "Id";
            string dateBegin = d.Cells[6].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[2] + "-"
                + d.Cells[6].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[1] + "-"
                + d.Cells[6].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[0];
            string dateEnd = d.Cells[7].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[2] + "-"
                + d.Cells[7].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[1] + "-"
                + d.Cells[7].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[0];
            string str = d.Cells[4].Value.ToString() + " проект с бюджетом "
                + d.Cells[5].Value.ToString().Substring(0, d.Cells[5].Value.ToString().Length - 2).Replace(",", ".") + " "
                + dateBegin + "-" + dateEnd
                + ". " + d.Cells[8].Value.ToString();
            РольПроект.SelectedIndex = РольПроект.FindStringExact(str);
        }
        public void ShowTechsProj(DataGridViewRow d)
        {
            ТехнПроектаВид.DataSource = DataS("Код, Название", "[Виды технологий]");
            ТехнПроектаВид.DisplayMember = "Name";
            ТехнПроектаВид.ValueMember = "Id";
            ТехнПроектаВид.SelectedIndex = ТехнПроектаВид.FindStringExact(d.Cells[1].Value.ToString());

            ТехнПроектПроект.DataSource = DataS("Проекты.Код, CONCAT(Тип, ' проект с бюджетом ', Бюджет, ' ', [Дата начала], '-', [Дата окончания], '. ', Заказчики.Наименование)", "Проекты LEFT JOIN Заказчики ON Заказчики.Код = [Код заказчика]");
            ТехнПроектПроект.DisplayMember = "Name";
            ТехнПроектПроект.ValueMember = "Id";
            string dateBegin = d.Cells[4].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[2] + "-"
                + d.Cells[4].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[1] + "-"
                + d.Cells[4].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[0];
            string dateEnd = d.Cells[5].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[2] + "-"
                + d.Cells[5].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[1] + "-"
                + d.Cells[5].Value.ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[0];
            string str = d.Cells[2].Value.ToString() + " проект с бюджетом "
                + d.Cells[3].Value.ToString().Substring(0, d.Cells[3].Value.ToString().Length - 2).Replace(",", ".") + " "
                + dateBegin + "-" + dateEnd
                + ". " + d.Cells[6].Value.ToString();
            ТехнПроектПроект.SelectedIndex = ТехнПроектПроект.FindStringExact(str);
        }
        public void ShowTechs(DataGridViewRow d)
        {
            ТехнологияНазвание.Text = d.Cells[1].Value.ToString();
        }
        public void ShowTraineers(DataGridViewRow d)
        {
            СтажёрФИО.Text = d.Cells[1].Value.ToString();
            СтажёрКуратор.DataSource = DataS("Код, CONCAT(ФИО, ' ', Телефон)", "Сотрудники");
            СтажёрКуратор.DisplayMember = "Name";
            СтажёрКуратор.ValueMember = "Id";
            СтажёрКуратор.SelectedIndex = СтажёрКуратор.FindStringExact(d.Cells[2].Value.ToString() + " " + d.Cells[3].Value.ToString());
        }
        public void ShowTrainig(DataGridViewRow d)
        {
            СтажировкаСтажёр.DataSource = DataS("Код, ФИО", "Стажирующиеся");
            СтажировкаСтажёр.DisplayMember = "Name";
            СтажировкаСтажёр.ValueMember = "Id";
            СтажировкаСтажёр.SelectedIndex = СтажировкаСтажёр.FindStringExact(d.Cells[1].Value.ToString());

            /*СтажировкаКуратор.DataSource = DataS("Код, CONCAT(ФИО, ' ', Телефон)", "Сотрудники");
            СтажировкаКуратор.DisplayMember = "Name";
            СтажировкаКуратор.ValueMember = "Id";
            СтажировкаКуратор.SelectedIndex = СтажировкаКуратор.FindStringExact(d.Cells[2].Value.ToString() + " " + d.Cells[3].Value.ToString());*/

            СтажировкаТест.Text = d.Cells[4].Value.ToString();
            СтажировкаДатаТеста.Text = d.Cells[5].Value.ToString();

            СтажировкаКурс.DataSource = DataS("[Курсы при компании].Код, CONCAT([Курсы при компании].Название, '(', [Виды технологий].Название, ')')", "[Курсы при компании] INNER JOIN [Виды технологий] ON [Код технологии] = [Виды технологий].Код");
            СтажировкаКурс.DisplayMember = "Name";
            СтажировкаКурс.ValueMember = "Id";
            СтажировкаКурс.SelectedIndex = СтажировкаКурс.FindStringExact(d.Cells[6].Value.ToString() + "(" + d.Cells[7].Value.ToString() + ")");
        }

        public void ShowCourses(DataGridViewRow d)
        {
            КурсНазвание.Text = d.Cells[1].Value.ToString();
            КурсДатаНачала.Text = d.Cells[2].Value.ToString();
            КурсДатаКонца.Text = d.Cells[3].Value.ToString();
            КурсТехнология.DataSource = DataS("Код, Название", "[Виды технологий]");
            КурсТехнология.DisplayMember = "Name";
            КурсТехнология.ValueMember = "Id";
            КурсТехнология.SelectedIndex = КурсТехнология.FindStringExact(d.Cells[4].Value.ToString());
        }
        public List<Info> DataS(string s1, string tbl)
        {
            List<Info> l = new List<Info>();
            string sqlExpression = "SELECT " + s1 + " FROM " + tbl + " ORDER BY Код";
            int count = (s1.Length - s1.Replace(",", "").Length) + 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        l.Add(new Info(Convert.ToInt32(reader[0]), reader[1].ToString()));
                    }
                }
                reader.Close();
            }
            return l;
        }

        public void HideAll()
        {
            foreach (var groupBox in Controls.OfType<GroupBox>())
            {
                groupBox.Visible = false;
            }
        }

        public void ClearAll()
        {
            foreach (var groupBox in Controls.OfType<GroupBox>())
            {
                foreach (var tb in groupBox.Controls.OfType<TextBox>())
                {
                    tb.Text = "";
                }
                foreach (var cb in groupBox.Controls.OfType<ComboBox>())
                {
                    cb.SelectedIndex = -1;
                }
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            if (st.EndsWith("Change"))
            {
                string tbl = st.Replace("Change", "");

                /*
                 * TODO CHECK FIELDS
                 * bool res = Check(tbl);*/
                bool res = true;
                string num = "", num1 = "", set = "";
                if (res)
                {
                    switch (tbl)
                    {
                        case "Страны":
                            set += "Название = '" + СтранаИмя.Text + "'";
                            break;
                        case "Заказчики":
                            set += "Наименование = '" + ЗаказчикИмя.Text + "', [Код страны] = " + ((Info)(ЗаказчикСтрана.SelectedItem)).Id.ToString();
                            break;
                        case "Проекты":
                            if (ПроектНачало.Value >= ПроектКонец.Value && (ПроектКонец.Value.ToString() != "" && ПроектКонец.Value.ToString() != ""))
                            {
                                MessageBox.Show("Дата начала проекта не может быть больше, чем дата его окончания");
                                return;
                            }
                            if (ПроектТип.Text != "Внутренний" && ПроектТип.Text != "Внешний")
                            {
                                MessageBox.Show("Проект может быть только внутренний или внешний");
                                return;
                            }
                            double unsenseParam;
                            if (!(double.TryParse(ПроектБюджет.Text.Replace('.', ','), out unsenseParam)))
                            {
                                MessageBox.Show("Бюджет проекта должен быть числом");
                                return;
                            }

                            bool allRight = false;

                            if (ПроектЗаказчик.Text.Trim() != "")
                            {
                                string query = "SELECT Наименование FROM Заказчики";

                                using (SqlConnection connection = new SqlConnection(connectionString))
                                {
                                    connection.Open();

                                    SqlCommand command = new SqlCommand(query, connection);
                                    SqlDataReader reader = command.ExecuteReader();

                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            if (reader.GetString(0) == ПроектЗаказчик.Text)
                                            {
                                                allRight = true;
                                            }
                                        }
                                    }
                                }
                            } else
                            {
                                allRight = true;
                            }
                            if (allRight == false)
                            {
                                MessageBox.Show("Выберите заказчика или оставьте поле пустым");
                                return;
                            }
                            string date = ПроектНачало.Value.Year.ToString() + "-" + ПроектНачало.Value.Month.ToString() + "-" + ПроектНачало.Value.Day.ToString();
                            string date2 = ПроектКонец.Value.Year.ToString() + "-" + ПроектКонец.Value.Month.ToString() + "-" + ПроектКонец.Value.Day.ToString();
                            set += "Тип = '" + ПроектТип.Text + "'" + ", Бюджет = " + ПроектБюджет.Text + ", [Дата начала] = '" + date + "', [Дата окончания] = '"
                                + date2 + "', [Код заказчика] = " + (ПроектЗаказчик.Text != "" ? ((Info)(ПроектЗаказчик.SelectedItem)).Id.ToString() : "NULL");
                            break;
                        case "Сотрудники":
                            string datePriem = СотрудникПриём.Value.Year.ToString() + "-" + СотрудникПриём.Value.Month.ToString() + "-" + СотрудникПриём.Value.Day.ToString();
                            string dateUvol = СотрудникУвол.Value.Year.ToString() + "-" + СотрудникУвол.Value.Month.ToString() + "-" + СотрудникУвол.Value.Day.ToString();
                            set += "Телефон = '" + СотрудникТелефон.Text + "'" + ", [Дата приёма] = '" + datePriem + "', [Дата увольнения] = '"
                                + dateUvol + "', ФИО = '" + СотрудникФИО.Text + "'";
                            break;
                        case "Роль в проекте":
                            set += "Наименование = '" + РольНазвание.Text + "'" + ", [Код сотрудника] = " + ((Info)(РольСотрудник.SelectedItem)).Id.ToString()
                                + ", [Код проекта] = " + ((Info)(РольПроект.SelectedItem)).Id.ToString();
                            break;
                        case "Технологии в проекте":
                            set += "[Код технологии] = " + ((Info)(ТехнПроектаВид.SelectedItem)).Id.ToString() + ", [Код проекта] = " + ((Info)(ТехнПроектПроект.SelectedItem)).Id.ToString();
                            break;
                        case "Виды технологий":
                            set += "Название = '" + ТехнологияНазвание.Text + "'";
                            break;
                        case "Стажирующиеся":
                            set += "ФИО = '" + СтажёрФИО.Text + "', [Код сотрудника] = " + ((Info)(СтажёрКуратор.SelectedItem)).Id.ToString();
                            break;
                        case "Стажировка":
                            string dateTest = СтажировкаДатаТеста.Value.Year.ToString() + "-" + СтажировкаДатаТеста.Value.Month.ToString() + "-" + СтажировкаДатаТеста.Value.Day.ToString();
                            set += "[Код стажёра] = " + ((Info)(СтажировкаСтажёр.SelectedItem)).Id.ToString() + ", [Результат теста] = " + СтажировкаТест.Text
                                + ", [Дата теста] = '" + dateTest + "', [Код курса] = " + ((Info)(СтажировкаКурс.SelectedItem)).Id.ToString();
                            break;
                        case "Курсы при компании":
                            string dateBegin = КурсДатаНачала.Value.Year.ToString() + "-" + КурсДатаНачала.Value.Month.ToString() + "-" + КурсДатаНачала.Value.Day.ToString();
                            string dateEnd = КурсДатаКонца.Value.Year.ToString() + "-" + КурсДатаКонца.Value.Month.ToString() + "-" + КурсДатаКонца.Value.Day.ToString();
                            set += "Название = '" + КурсНазвание.Text + "'" + ", [Дата начала] = '" + dateBegin
                                + "', [Дата окончания] = '" + dateEnd + "', [Код технологии] = " + ((Info)(КурсТехнология.SelectedItem)).Id.ToString();
                            break;

                    }
                    try
                    {
                        string query = "UPDATE [" + tbl + "] SET " + set + " WHERE Код = " + DataId.ToString();

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(query, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                        }
                        this.Close();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("Заполните все поля правильно!");
                }
            }
            else
            {
                string tbl = st.Replace("Add", "");

                /*bool res = Check(tbl);*/
                bool res = true;
                string num = "", num1 = "", fields = "", values = "";
                if (res)
                {
                    switch (tbl)
                    {
                        case "Заказчики":
                            fields += "[Код страны], Наименование";
                            values += ((Info)(ЗаказчикСтрана.SelectedItem)).Id.ToString() + ", '" + ЗаказчикИмя.Text + "'";
                            break;
                        case "Страны":
                            fields += "Название";
                            values += "'" + СтранаИмя.Text + "'";
                            break;
                        case "Проекты":
                            string date = ПроектНачало.Value.Year.ToString() + "-" + ПроектНачало.Value.Month.ToString() + "-" + ПроектНачало.Value.Day.ToString();
                            string date2 = ПроектКонец.Value.Year.ToString() + "-" + ПроектКонец.Value.Month.ToString() + "-" + ПроектКонец.Value.Day.ToString();
                            fields += "Тип, Бюджет, [Дата начала], [Дата окончания], [Код заказчика]";
                            values += "'" + ПроектТип.Text + "', " + ПроектБюджет.Text + ", '" + date + "', '" + date2 + "', " + (ПроектЗаказчик.Text != "" ? ((Info)(ПроектЗаказчик.SelectedItem)).Id.ToString() : "NULL");
                            break;
                        case "Сотрудники":
                            string datePriem = СотрудникПриём.Value.Year.ToString() + "-" + СотрудникПриём.Value.Month.ToString() + "-" + СотрудникПриём.Value.Day.ToString();
                            string dateUvol = СотрудникУвол.Value.Year.ToString() + "-" + СотрудникУвол.Value.Month.ToString() + "-" + СотрудникУвол.Value.Day.ToString();
                            fields += "Телефон, ФИО, [Дата приёма], [Дата увольнения]";
                            values += "'" + СотрудникТелефон.Text + "', '" + СотрудникФИО.Text + "', '" + datePriem + "', '" + dateUvol + "'";
                            break;
                        case "Роль в проекте":
                            fields += "Наименование, [Код сотрудника], [Код проекта]";
                            values += "'" + РольНазвание.Text + "', " + ((Info)(РольСотрудник.SelectedItem)).Id.ToString() + ", " + ((Info)(РольПроект.SelectedItem)).Id.ToString();
                            break;
                        case "Технологии в проекте":
                            fields += "[Код технологии], [Код проекта]";
                            values += "'" + ((Info)(ТехнПроектаВид.SelectedItem)).Id.ToString() + "', '" + ((Info)(ТехнПроектПроект.SelectedItem)).Id.ToString() + "'";
                            break;
                        case "Стажирующиеся":
                            fields += "ФИО, [Код сотрудника]";
                            values += "'" + СтажёрФИО.Text + "', " + ((Info)(СтажёрКуратор.SelectedItem)).Id.ToString().ToString();
                            break;
                        case "Стажировка":
                            string dateTest = СтажировкаДатаТеста.Value.Year.ToString() + "-" + СтажировкаДатаТеста.Value.Month.ToString() + "-" + СтажировкаДатаТеста.Value.Day.ToString();
                            fields += "[Код стажёра], [Результат теста], [Дата теста], [Код курса]";
                            string kodStajera;
                            try
                            {
                                kodStajera = ((Info)(СтажировкаСтажёр.SelectedItem)).Id.ToString();
                            } catch
                            {
                                kodStajera = "-1";
                            }
                            values += kodStajera + ", " + СтажировкаТест.Text + ", '" + dateTest + "', " + ((Info)(СтажировкаКурс.SelectedItem)).Id.ToString();
                            break;
                        case "Курсы при компании":
                            string dateBegin = КурсДатаНачала.Value.Year.ToString() + "-" + КурсДатаНачала.Value.Month.ToString() + "-" + КурсДатаНачала.Value.Day.ToString();
                            string dateEnd = КурсДатаКонца.Value.Year.ToString() + "-" + КурсДатаКонца.Value.Month.ToString() + "-" + КурсДатаКонца.Value.Day.ToString();
                            fields += "Название, [Дата начала], [Дата окончания], [Код технологии]";
                            values += "'" + КурсНазвание.Text + "', '" + dateBegin + "', '" + dateEnd + "', " + ((Info)(КурсТехнология.SelectedItem)).Id.ToString();
                            break;
                        case "Виды технологий":
                            fields += "Название";
                            values += "'" + ТехнологияНазвание.Text + "'";
                            break;
                    }
                    try
                    {
                        string sql = "INSERT INTO [" + tbl + "] (" + fields + ") VALUES (" + values + ")";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            adapter = new SqlDataAdapter(sql, connection);

                            ds = new DataSet();
                            adapter.Fill(ds);
                        }
                        this.Close();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }

            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label41_Click(object sender, EventArgs e)
        {

        }

        private void groupBox8_Enter(object sender, EventArgs e)
        {

        }
    }
}
