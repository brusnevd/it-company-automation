﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Kursach
{
    public partial class Diagram : Form
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        public Diagram()
        {
            InitializeComponent();
        }

        private void Diagram_Load(object sender, EventArgs e)
        {
            chart1.Titles.Add("Бюджеты заказчиков");
            chart1.Titles[0].Font = new Font("Utopia", 16);

            string query = "SELECT Бюджет, Наименование FROM БюджетыЗаказчиков";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();

                int i = 0;

                chart1.Series.Add(new Series("Суммарный бюджет")
                {
                    ChartType = SeriesChartType.StackedBar
                });
                chart1.Series[0].Points.DataBindXY(reader, "Наименование", reader, "Бюджет");

                reader.Close();
            }
        }
    }
}
