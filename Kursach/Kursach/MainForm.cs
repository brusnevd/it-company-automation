﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 
using Excel = Microsoft.Office.Interop.Excel;

namespace Kursach
{
    public partial class MainForm : Form
    {
        DataSet ds;
        SqlDataAdapter adapter;
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        string table = "Сотрудники";
        string sql;
        Form1 f;
        public MainForm(Form1 f)
        {
            this.f = f;
            InitializeComponent();
            if (f.login == "admin")
            {
                radioButton11.Enabled = true;
                radioButton11.Visible = true;
                журналИзмененийToolStripMenuItem.Visible = true;
            }
            if (f.login == "buhgalter")
            {
                отчётыToolStripMenuItem.Visible = true;
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            RadioButton radioButton = (RadioButton)sender;
            if (radioButton.Text == "Пользователи")
            {
                button1.Enabled = false;
                button2.Enabled = false;
            }
            if (radioButton.Checked)
            {
                table = radioButton.Text;
            }
            Reload();
        }

        public void Reload()
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;

            switch (table)
            {
                case "Заказчики":
                    sql = "SELECT Код, Наименование, Страна FROM ЗаказчикиСоСтраннами ORDER BY Код";
                    break;
                case "Проекты":
                    sql = "SELECT Проекты.Код, Тип, Бюджет, [Дата начала], [Дата окончания], Наименование as Заказчик FROM Заказчики RIGHT JOIN Проекты ON " +
                        "Заказчики.Код = Проекты.[Код заказчика] ORDER BY Проекты.Код";
                    break;
                case "Роль в проекте":
                    sql = "SELECT [Роль в проекте].Код, [Роль в проекте].Наименование, ФИО, Телефон as [Телефон сотрудника], Тип as [Тип проекта], Бюджет, [Дата начала], [Дата окончания], " +
                        "Заказчики.Наименование as Заказчик FROM Проекты LEFT JOIN Заказчики ON Заказчики.Код = Проекты.[Код заказчика], [Роль в проекте], Сотрудники " +
                        "WHERE Сотрудники.Код = [Код сотрудника] AND Проекты.Код = [Код проекта] " +
                        "ORDER BY [Роль в проекте].Код";
                    break;
                case "Стажирующиеся":
                    sql = "SELECT Стажирующиеся.Код, Стажирующиеся.ФИО, Сотрудники.ФИО as Куратор, Телефон as [Телефон куратора] FROM Сотрудники, Стажирующиеся " +
                        "WHERE Сотрудники.Код = [Код сотрудника] " +
                        "ORDER BY Стажирующиеся.Код";
                    break;
                case "Стажировка":
                    sql = "SELECT Стажировка.Код, Стажирующиеся.ФИО AS [ФИО Стажёра], Сотрудники.ФИО AS [Куратор стажёра], Телефон, [Результат теста], [Дата теста], " +
                        "[Курсы при компании].Название AS [Название курса], [Виды технологий].Название AS [Курс по технологии] " +
                        "FROM Стажировка, Стажирующиеся, [Курсы при компании], [Виды технологий], Сотрудники " +
                        "WHERE [Код стажёра] = Стажирующиеся.Код AND [Код курса] = [Курсы при компании].Код AND [Код технологии] = [Виды технологий].Код AND [Код сотрудника] = Сотрудники.Код " +
                        "ORDER BY Стажировка.Код";
                    break;
                case "Курсы при компании":
                    sql = "SELECT * FROM КурсыБезКодов ORDER BY КурсыБезКодов.Код";
                    break;
                case "Технологии в проекте":
                    sql = "SELECT [Технологии в проекте].Код, [Виды технологий].Название AS [Название технологии], Тип AS [Тип проекта], Бюджет, [Дата начала], [Дата окончания], " +
                        "Заказчики.Наименование AS Заказчик " +
                        "FROM [Технологии в проекте], [Виды технологий], Проекты LEFT JOIN Заказчики ON Заказчики.Код = Проекты.[Код заказчика]" +
                        "WHERE [Виды технологий].Код = [Код технологии] AND Проекты.Код = [Код проекта] " +
                        "ORDER BY[Технологии в проекте].Код";
                    break;
                case "Пользователи":
                    sql = "SELECT Код, Почта, Логин FROM users WHERE Логин <> 'admin'";
                    break;
                default:
                    sql = "SELECT * FROM [" + table + "] ORDER BY Код";
                    break;
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);
                ds = new DataSet();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                // делаем недоступным столбец id для изменения
                dataGridView1.Columns["Код"].ReadOnly = true;
                dataGridView1.Columns["Код"].Visible = false;
                if (dataGridView1.Rows.Count == 0)
                {
                    button3.Enabled = false;
                    button2.Enabled = false;
                }
            }
        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int id;
                sql = "DELETE FROM [" + table + "] WHERE Код IN (-1";
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    id = Convert.ToInt32(row.Cells[0].Value);
                    sql += ", " + id;
                }
                sql += ")";

                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        adapter = new SqlDataAdapter(sql, connection);

                        ds = new DataSet();
                        adapter.Fill(ds);
                    }
                    Reload();
                }
                catch (SqlException ex)
                {
                    string str = ex.Message.ToString();
                    MessageBox.Show(str.Substring(0, str.IndexOf('\n')));
                    // MessageBox.Show("Ошибка удаления");
                }
            }
            else
            {
                MessageBox.Show("Выберите запись(-и) для удаления");
            }
        }


        private void printClickHandler(object sender, EventArgs e)
        {
            string path = string.Empty;
            using (SaveFileDialog saveDialog = new SaveFileDialog())
            {
                saveDialog.Filter = "Файлы Excel (*.xls; *.xlsx) | *.xls; *.xlsx";
                saveDialog.DefaultExt = ".xls";
                saveDialog.FileName = table;
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    path = saveDialog.FileName;
                    Excel.Application xlApp = new Excel.Application();

                    if (xlApp == null)
                    {
                        MessageBox.Show("Excel is not properly installed!!");
                        return;
                    }
                    object misValue = Missing.Value;
                    Excel.Workbook xlWorkBook;
                    Excel.Worksheet xlWorkSheet;

                    xlWorkBook = xlApp.Workbooks.Add(misValue);

                    if (File.Exists(path))
                    {
                        Excel.Workbook xlWorkBook1 = xlApp.Workbooks.Open(path, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                        Excel.Sheets worksheets = xlWorkBook1.Worksheets;
                        var xlSheets = xlWorkBook1.Sheets as Excel.Sheets;
                        var xlNewSheet = (Excel.Worksheet)xlSheets.Add(xlSheets[1], Type.Missing, Type.Missing, Type.Missing);
                        /// xlSheets[2].Delete();
                        xlWorkBook1.Save();
                        xlWorkBook1.Close();
                    }

                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    xlWorkSheet.Name = "Отчет за " + (DateTime.Now.Day.ToString().Length == 1 ? ("0" + DateTime.Now.Day.ToString()) : DateTime.Now.Day.ToString()) + "." + (DateTime.Now.Month.ToString().Length == 1 ? ("0" + DateTime.Now.Month.ToString()) : DateTime.Now.Month.ToString()) + "." + DateTime.Now.Year;

                    for (int k = 2; k < dataGridView1.ColumnCount + 1; k++)
                    {
                        xlWorkSheet.Cells[1, k] = dataGridView1.Columns[k - 1].HeaderText;
                        xlWorkSheet.Cells.Font.Name = "Tahoma";
                    }

                    for (int i = 1; i < dataGridView1.Rows.Count + 1; i++)
                    {
                        for (int j = 2; j < dataGridView1.ColumnCount + 1; j++)
                        {
                            xlWorkSheet.Cells[i + 1, j] = (String)dataGridView1.Rows[i - 1].Cells[j - 1].Value.ToString();
                        }
                    }

                    Excel.Range range1 = xlWorkSheet.get_Range(xlWorkSheet.Cells[2, 2], xlWorkSheet.Cells[dataGridView1.Rows.Count + 1, dataGridView1.ColumnCount]);

                    Excel.Range range2 = xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 2], xlWorkSheet.Cells[1, dataGridView1.ColumnCount]);

                    range1.EntireColumn.AutoFit();
                    range1.EntireRow.AutoFit();

                    range2.EntireColumn.AutoFit();
                    range2.EntireRow.AutoFit();

                    range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous;
                    range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous;
                    range1.Borders.get_Item(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous;
                    range1.Borders.get_Item(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous;
                    range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous;

                    range1.Borders.Color = ColorTranslator.ToOle(Color.Blue);
                    range2.Borders.Color = ColorTranslator.ToOle(Color.Red);

                    xlWorkBook.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);
                    MessageBox.Show("Успешно сохранено в файл!");
                }
            }
        }

        private void infoClickHandler(object sender, EventArgs e)
        {
            // filter
            Search f = new Search();
            f.ShowDialog();
        }

        private void searchClickHandler(object sender, EventArgs e)
        {
            MessageBox.Show("Search");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            /*ToolStripMenuItem aboutItem = new ToolStripMenuItem("Печать таблицы");
            aboutItem.Click += printClickHandler;
            menuStrip1.Items.Add(aboutItem);

            ToolStripMenuItem aboutItem2 = new ToolStripMenuItem("Информация");
            aboutItem2.Click += infoClickHandler;
            menuStrip1.Items.Add(aboutItem2);

            ToolStripMenuItem aboutItem3 = new ToolStripMenuItem("Поиск");
            aboutItem3.Click += searchClickHandler;
            menuStrip1.Items.Add(aboutItem3);*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int c = dataGridView1.SelectedRows.Count;
            int id = 0;
            if (c == 1)
            {
                if (dataGridView1.CurrentRow == null)
                {
                    id = 0;
                }
                else id = Convert.ToInt32(dataGridView1.CurrentRow.Index);

                AddChangeForm f = new AddChangeForm(table, dataGridView1.SelectedRows[0]);
                f.ShowDialog();
                Reload();
            }
            else if (c == 0)
            {
                MessageBox.Show("Выберите запись для изменения");
            }
            else
            {
                MessageBox.Show("Для изменения можно выбрать только одну запись");
            }
            dataGridView1.CurrentCell = dataGridView1.Rows[id].Cells[1];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddChangeForm f = new AddChangeForm(table);
            f.ShowDialog();
            Reload();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.f.Show();
        }

        private void выйтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.f.Show();
        }

        private void годовойОтчётБюджетаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // отчёт о бюджете за выбранный период
            BudgetReport f = new BudgetReport();
            f.ShowDialog();
        }

        private void общийБюджетПроектовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Группирующий отчёт о заказчиках
            /*CustomerReport f = new CustomerReport();
            f.ShowDialog();*/
            string sqlExpression = "SELECT * FROM КоличествоПроектовЗаказчиков";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    string path = string.Empty;
                    using (SaveFileDialog saveDialog = new SaveFileDialog())
                    {
                        saveDialog.Filter = "Файлы Excel (*.xls; *.xlsx) | *.xls; *.xlsx";
                        saveDialog.DefaultExt = ".xls";
                        saveDialog.FileName = "Количество проектов от каждого заказчика";
                        if (saveDialog.ShowDialog() == DialogResult.OK)
                        {
                            path = saveDialog.FileName;
                            Excel.Application xlApp = new Excel.Application();

                            if (xlApp == null)
                            {
                                MessageBox.Show("Excel is not properly installed!!");
                                return;
                            }
                            object misValue = Missing.Value;
                            Excel.Workbook xlWorkBook;
                            Excel.Worksheet xlWorkSheet;

                            xlWorkBook = xlApp.Workbooks.Add(misValue);

                            if (File.Exists(path))
                            {
                                Excel.Workbook xlWorkBook1 = xlApp.Workbooks.Open(path, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                                Excel.Sheets worksheets = xlWorkBook1.Worksheets;
                                var xlSheets = xlWorkBook1.Sheets as Excel.Sheets;
                                var xlNewSheet = (Excel.Worksheet)xlSheets.Add(xlSheets[1], Type.Missing, Type.Missing, Type.Missing);
                                /// xlSheets[2].Delete();
                                xlWorkBook1.Save();
                                xlWorkBook1.Close();
                            }

                            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                            xlWorkSheet.Name = "Отчет за " + (DateTime.Now.Day.ToString().Length == 1 ? ("0" + DateTime.Now.Day.ToString()) : DateTime.Now.Day.ToString()) + "." + (DateTime.Now.Month.ToString().Length == 1 ? ("0" + DateTime.Now.Month.ToString()) : DateTime.Now.Month.ToString()) + "." + DateTime.Now.Year;
                            String[] arr = new string[] { "Заказчик", "Количество проектов" };
                            for (int k = 2; k < arr.Length + 1; k++)
                            {
                                xlWorkSheet.Cells[1, k] = arr[k - 1];
                                xlWorkSheet.Cells.Font.Name = "Tahoma";
                            }

                            int i = 1;
                            xlWorkSheet.Cells[i, 2] = "Заказчик";
                            xlWorkSheet.Cells[i, 3] = "Количество проектов";

                            decimal sum = 0;

                            while (reader.Read())
                            {
                                xlWorkSheet.Cells[i + 1, 2] = reader.GetString(0);
                                xlWorkSheet.Cells[i + 1, 3] = reader[1].ToString();
                                i += 1;
                            }

                            Excel.Range range1 = xlWorkSheet.get_Range(xlWorkSheet.Cells[2, 2], xlWorkSheet.Cells[i, 3]);

                            Excel.Range range2 = xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 2], xlWorkSheet.Cells[1, 3]);

                            range1.EntireColumn.AutoFit();
                            range1.EntireRow.AutoFit();

                            range2.EntireColumn.AutoFit();
                            range2.EntireRow.AutoFit();

                            range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous;

                            range1.Borders.Color = ColorTranslator.ToOle(Color.Blue);
                            range2.Borders.Color = ColorTranslator.ToOle(Color.Red);

                            xlWorkBook.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                            xlWorkBook.Close(true, misValue, misValue);
                            xlApp.Quit();

                            Marshal.ReleaseComObject(xlWorkSheet);
                            Marshal.ReleaseComObject(xlWorkBook);
                            Marshal.ReleaseComObject(xlApp);
                            MessageBox.Show("Успешно сохранено в файл!");
                        }
                    }
                }
                reader.Close();
            }
        }

        private void диаграммыToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void катировкиПроектовЗаГодToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Diagram d = new Diagram();
            d.ShowDialog();
        }

        private void журналИзмененийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Journal j = new Journal(dataGridView1);

            j.ShowDialog();
        }
    }
}
