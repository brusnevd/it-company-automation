﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach
{
    public partial class Form1 : Form
    {
        public string login;
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        bool CheckPassword = false;

        public Form1()
        {
            InitializeComponent();
            this.Focus();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*this.Hide();*/
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!CheckPassword)
            {
                logPassword.PasswordChar = '\0';
                checkBox1.Checked = true;
                CheckPassword = true;
            }
            else
            {
                logPassword.PasswordChar = '*';
                checkBox1.Checked = false;
                CheckPassword = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Registration registrationForm = new Registration();
            this.Hide();
            registrationForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (logLogin.Text != "" && logPassword.Text != "")
            {
                // TODO Log-in
                string sqlExpression = "SELECT * FROM users WHERE Логин= '" + logLogin.Text + "' and Пароль = '" + logPassword.Text + "'";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        this.login = logLogin.Text;
                        logLogin.Text = "";
                        logPassword.Text = "";
                        MainForm f = new MainForm(this);
                        this.Hide();
                        f.Show();
                    }
                    else
                    {
                        MessageBox.Show("Неверный логин или пароль");
                    }
                    reader.Close();
                }
            } else
            {
                MessageBox.Show("Заполните поля помеченные - *");
            }
        }
    }
}
