﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach
{
    public partial class Registration : Form
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        bool CheckPassword = false;
        DataSet ds;
        SqlDataAdapter adapter;
        public Registration()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!CheckPassword)
            {
                inputPassword.PasswordChar = '\0';
                checkBox1.Checked = true;
                CheckPassword = true;
            }
            else
            {
                inputPassword.PasswordChar = '*';
                checkBox1.Checked = false;
                CheckPassword = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (inputLogin.Text != "" && inputMail.Text != "" && inputPassword.Text != "")
            {
                // TODO - Database work
                string sqlExpression = "SELECT * FROM users WHERE Логин = '" + inputLogin.Text + "'";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        MessageBox.Show("Логин занят");
                    } else {
                        reader.Close();
                        string query = "INSERT INTO Users (Почта, Логин, Пароль) VALUES ('" + inputMail.Text + "', '" + inputLogin.Text + "', '" + inputPassword.Text + "')";
                        try
                        {
                            using (SqlConnection conn = new SqlConnection(connectionString))
                            {
                                conn.Open();
                                adapter = new SqlDataAdapter(query, connection);

                                ds = new DataSet();
                                adapter.Fill(ds);
                            }
                            inputLogin.Text = "";
                            inputMail.Text = "";
                            inputPassword.Text = "";
                            MessageBox.Show("Вы успешно зарегстрированы");
                            Form1 f = new Form1();
                            f.Show();
                            this.Close();
                        }
                        catch (SqlException ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                    reader.Close();
                }
            } else
            {
                MessageBox.Show("Заполните поля помеченные - *");
            }
        }
    }
}
