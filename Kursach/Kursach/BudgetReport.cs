﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Kursach
{
    public partial class BudgetReport : Form
    {
        DataSet ds;
        SqlDataAdapter adapter;
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        public BudgetReport()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string date = с.Value.Year.ToString() + "-" + с.Value.Month.ToString() + "-" + с.Value.Day.ToString();
            string date2 = по.Value.Year.ToString() + "-" + по.Value.Month.ToString() + "-" + по.Value.Day.ToString();
            string sqlExpression = "EXEC ПроектыПоДате '" + date + "', '" + date2 + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    string path = string.Empty;
                    using (SaveFileDialog saveDialog = new SaveFileDialog())
                    {
                        saveDialog.Filter = "Файлы Excel (*.xls; *.xlsx) | *.xls; *.xlsx";
                        saveDialog.DefaultExt = ".xls";
                        saveDialog.FileName = "Очёт о бюджете";
                        if (saveDialog.ShowDialog() == DialogResult.OK)
                        {
                            path = saveDialog.FileName;
                            Excel.Application xlApp = new Excel.Application();

                            if (xlApp == null)
                            {
                                MessageBox.Show("Excel is not properly installed!!");
                                return;
                            }
                            object misValue = Missing.Value;
                            Excel.Workbook xlWorkBook;
                            Excel.Worksheet xlWorkSheet;

                            xlWorkBook = xlApp.Workbooks.Add(misValue);

                            if (File.Exists(path))
                            {
                                Excel.Workbook xlWorkBook1 = xlApp.Workbooks.Open(path, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                                Excel.Sheets worksheets = xlWorkBook1.Worksheets;
                                var xlSheets = xlWorkBook1.Sheets as Excel.Sheets;
                                var xlNewSheet = (Excel.Worksheet)xlSheets.Add(xlSheets[1], Type.Missing, Type.Missing, Type.Missing);
                                /// xlSheets[2].Delete();
                                xlWorkBook1.Save();
                                xlWorkBook1.Close();
                            }

                            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                            xlWorkSheet.Name = "Отчет за " + (DateTime.Now.Day.ToString().Length == 1 ? ("0" + DateTime.Now.Day.ToString()) : DateTime.Now.Day.ToString()) + "." + (DateTime.Now.Month.ToString().Length == 1 ? ("0" + DateTime.Now.Month.ToString()) : DateTime.Now.Month.ToString()) + "." + DateTime.Now.Year;
                            String[] arr = new string[] { "Тип", "Бюджет", "Дата начала", "Дата окончания", "Заказчик" };
                            for (int k = 2; k < 5 + 1; k++)
                            {
                                xlWorkSheet.Cells[1, k] = arr[k - 1];
                                xlWorkSheet.Cells.Font.Name = "Tahoma";
                            }

                            int i = 1;
                            xlWorkSheet.Cells[i, 2] = "Тип";
                            xlWorkSheet.Cells[i, 3] = "Бюджет";
                            xlWorkSheet.Cells[i, 4] = "Дата начала";
                            xlWorkSheet.Cells[i, 5] = "Дата окончания";
                            xlWorkSheet.Cells[i, 6] = "Заказчик";

                            decimal sum = 0;

                            while (reader.Read())
                            {
                                sum += reader.GetDecimal(1);
                                string dateBegin = reader.GetDateTime(2).ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[2] + "-"
                                    + reader.GetDateTime(2).ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[1] + "-"
                                    + reader.GetDateTime(2).ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[0];
                                string dateEnd = reader.GetDateTime(3).ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[2] + "-"
                                    + reader.GetDateTime(3).ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[1] + "-"
                                    + reader.GetDateTime(3).ToString().Split(new char[] { ' ' })[0].Split(new char[] { '.' })[0];
                                xlWorkSheet.Cells[i + 1, 2] = reader.GetString(0);
                                xlWorkSheet.Cells[i + 1, 3] = reader.GetDecimal(1).ToString();
                                xlWorkSheet.Cells[i + 1, 4] = dateBegin;
                                xlWorkSheet.Cells[i + 1, 5] = dateEnd;
                                xlWorkSheet.Cells[i + 1, 6] = reader.GetValue(4);
                                i += 1;
                            }

                            xlWorkSheet.Cells[i + 1, 2] = "Итого: ";
                            xlWorkSheet.Cells[i + 1, 3] = sum.ToString();

                            Excel.Range range1 = xlWorkSheet.get_Range(xlWorkSheet.Cells[2, 2], xlWorkSheet.Cells[i, 6]);

                            Excel.Range range2 = xlWorkSheet.get_Range(xlWorkSheet.Cells[1, 2], xlWorkSheet.Cells[1, 6]);

                            range1.EntireColumn.AutoFit();
                            range1.EntireRow.AutoFit();

                            range2.EntireColumn.AutoFit();
                            range2.EntireRow.AutoFit();

                            range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous;
                            range1.Borders.get_Item(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous;

                            range1.Borders.Color = ColorTranslator.ToOle(Color.Blue);
                            range2.Borders.Color = ColorTranslator.ToOle(Color.Red);

                            xlWorkBook.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                            xlWorkBook.Close(true, misValue, misValue);
                            xlApp.Quit();

                            Marshal.ReleaseComObject(xlWorkSheet);
                            Marshal.ReleaseComObject(xlWorkBook);
                            Marshal.ReleaseComObject(xlApp);
                            MessageBox.Show("Успешно сохранено в файл!");
                        }
                    }
                }
                reader.Close();
            }
        }
    }
}
