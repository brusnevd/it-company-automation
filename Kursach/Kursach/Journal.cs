﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Kursach
{
    public partial class Journal : Form
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        DataSet ds;
        SqlDataAdapter adapter;
        DataGridView f;
        public Journal(DataGridView d)
        {
            InitializeComponent();
            this.f = d;
        }

        private void Journal_Load(object sender, EventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;

            string sql = "SELECT * FROM КурсыLog";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);

                ds = new DataSet();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                // делаем недоступным столбец id для изменения
                //dataGridView1.Columns["Код"].ReadOnly = true;
                // dataGridView1.Columns["Код"].Visible = false;
            }
        }

        public DataSet sqlQueryResponse(string sql)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);

                ds = new DataSet();
                adapter.Fill(ds);
                return ds;
            }
        }
        public void sqlQuery(string sql)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int c = dataGridView1.SelectedRows.Count;
            int id = 0;
            if (c == 1)
            {
                if (dataGridView1.CurrentRow == null)
                {
                    id = 0;
                }
                else id = Convert.ToInt32(dataGridView1.CurrentRow.Index);

                string sql = "exec ОткатКурсы " + Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);

                MessageBox.Show(sql);

                sqlQuery(sql);

                DataSet ds = sqlQueryResponse("SELECT * FROM КурсыLog");

                dataGridView1.DataSource = ds.Tables[0];

                sql = "SELECT * FROM КурсыБезКодов ORDER BY КурсыБезКодов.Код";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    ds = new DataSet();
                    adapter.Fill(ds);

                    f.DataSource = ds.Tables[0];
                }
            }
            else if (c == 0)
            {
                MessageBox.Show("Выберите запись для отката изменений");
            }
            else
            {
                MessageBox.Show("Для отката изенений можно выбрать только одну запись");
            }
        }
    }
}
