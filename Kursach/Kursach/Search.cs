﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach
{
    public partial class Search : Form
    {
        DataSet ds;
        SqlDataAdapter adapter;
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ФирмаПО;Integrated Security=True";
        public Search()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    comboBox2.DropDownStyle = ComboBoxStyle.DropDown;
                    richTextBox1.Text = "Информация о проектах с заданным количеством сотрудников";
                    comboBox2.Visible = false;
                    textBox1.Visible = true;
                    break;
                case 1:
                    comboBox2.DropDownStyle = ComboBoxStyle.DropDown;
                    richTextBox1.Text = "Средний результат всех тестов стажёра";
                    comboBox2.Visible = true;
                    textBox1.Visible = false;
                    comboBox2.DataSource = DataS("Стажирующиеся.Код, CONCAT(Стажирующиеся.ФИО, '. Куратор - ', Сотрудники.ФИО)", "Стажирующиеся, Сотрудники WHERE Сотрудники.Код = [Код сотрудника]");
                    comboBox2.DisplayMember = "Name";
                    comboBox2.ValueMember = "Id";
                    comboBox2.SelectedIndex = 0;
                    break;
            }
        }

        public List<Info> DataS(string s1, string tbl)
        {
            List<Info> l = new List<Info>();
            string sqlExpression = "SELECT " + s1 + " FROM " + tbl + " ORDER BY Код";
            int count = (s1.Length - s1.Replace(",", "").Length) + 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        l.Add(new Info(Convert.ToInt32(reader[0]), reader[1].ToString()));
                    }
                }
                reader.Close();
            }
            return l;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "";
            if (comboBox1.SelectedIndex == 0)
            {
                sql = "SELECT * FROM dbo.ПроектыПоКоличеству(" + textBox1.Text + ")";
            }
            if (comboBox1.SelectedIndex == 1)
            {
                sql = "SELECT Код, ФИО, dbo.AvBall(" + ((Info)(comboBox2.SelectedItem)).Id.ToString() + ") AS [Средний балл] FROM Стажирующиеся WHERE Код = " + ((Info)(comboBox2.SelectedItem)).Id.ToString();
            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);

                ds = new DataSet();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView1.Columns["Код"].Visible = false;
            }
        }
    }
}
