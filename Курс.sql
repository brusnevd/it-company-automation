drop table users
GO
create table users (
	����� VARCHAR(40),
	����� VARCHAR(40),
	������ VARCHAR(40),
)
GO

insert into users VALUES ('admin', 'admin', 'admin')
GO
insert into users VALUES ('buhgalter', 'buhgalter', 'buhgalter')
GO

drop view ���������������
GO
create view ���������������
as
SELECT ���, ������, [���� ������], [���� ���������], ���������.������������
FROM ������� LEFT JOIN ��������� ON ���������.��� = [��� ���������]
GO

drop view ��������������������
GO
create view ��������������������
as
SELECT ���������.���, ������������, �������� as ������
FROM ���������, ������
WHERE ������.��� = [��� ������]
GO

drop proc �������������
GO
create proc ������������� @dateBegin DATE, @dateEnd DATE
AS
SELECT * FROM ���������������
WHERE [���� ������] > @dateBegin AND [���� ���������] < @dateEnd
GO
EXEC ������������� '2001.05.02', '2021-05-02'

SELECT ������
FROM �������

GO
drop view �����������������
GO
create view �����������������
AS
SELECT SUM(������) AS ������, ISNULL(������������, '���������� �������') As ������������
FROM ������� LEFT JOIN ��������� ON [��� ���������] = ���������.���
GROUP BY ������������

GO

SELECT * FROM [����� ��� ��������]

SELECT * FROM �����������������

DROP TABLE �����Log
GO
CREATE TABLE �����Log
( 
  id INT IDENTITY PRIMARY KEY
  , typeLog CHAR
  , dateLog DATETIME
  , userLog VARCHAR(100)
  , hostLog VARCHAR(100)

  , ��� INT
  , �������� VARCHAR(40)
  , [���� ������] DATE
  , [���� ���������] DATE
  , [��� ����������] INT
)
GO

drop trigger trg�����Log
GO
CREATE TRIGGER trg�����Log ON [����� ��� ��������]
  AFTER INSERT, UPDATE, DELETE
AS
DECLARE @datelog DATETIME
SET @datelog = GetDate()
INSERT INTO �����Log 
  SELECT 'D', @datelog, SYSTEM_USER, HOST_NAME(), ���, ��������, [���� ������], [���� ���������], [��� ����������] FROM DELETED
INSERT INTO �����Log 
  SELECT 'I', @datelog, SYSTEM_USER, HOST_NAME(),  ���, ��������, [���� ������], [���� ���������], [��� ����������] FROM INSERTED
GO

DROP PROC ����������
GO
CREATE PROC ���������� @to INT
AS
DECLARE CURS CURSOR FOR
  SELECT TypeLog, ���, ��������, [���� ������], [���� ���������], [��� ����������]
    FROM �����Log WHERE ID >= @to ORDER BY ID DESC
DECLARE @type CHAR, @kod INT, @title VARCHAR(40), @dateBegin DATE, @dateEnd DATE, @techId INT
SET IDENTITY_INSERT [����� ��� ��������] ON
OPEN CURS
FETCH CURS INTO @type, @kod, @title, @dateBegin, @dateEnd, @techId
WHILE @@FETCH_STATUS = 0
BEGIN
  IF @type = 'I'
    DELETE FROM [����� ��� ��������] WHERE ��� = @kod
  ELSE
    INSERT INTO [����� ��� ��������](���, ��������, [���� ������], [���� ���������], [��� ����������])
	  VALUES (@kod, @title, @dateBegin, @dateEnd, @techId)
  FETCH CURS INTO @type, @kod, @title, @dateBegin, @dateEnd, @techId
END
SET IDENTITY_INSERT [����� ��� ��������] OFF
CLOSE CURS
DEALLOCATE CURS
DELETE FROM �����Log WHERE ID >= @to
GO

drop view �������������
GO
create view �������������
AS
SELECT [����� ��� ��������].���, [����� ��� ��������].��������, [���� ������], [���� ���������], [���� ����������].�������� AS [���� �� ����������]
FROM [����� ��� ��������], [���� ����������]
WHERE [���� ����������].��� = [��� ����������]
GO

DROP VIEW ����������������������������
GO
CREATE VIEW ����������������������������
AS
SELECT ISNULL(������������, '���������� �������') AS ��������, COUNT(*) AS [���������� ��������]
FROM ���������������
GROUP BY ������������
GO

SELECT * FROM ����������������������������

GO

-- ������� �� ���������� ����������� � ���
DROP FUNCTION �������������������
GO
CREATE FUNCTION ������������������� (@kolvo INT)
RETURNS @res TABLE 
    (
	  ��� INT PRIMARY KEY
	  , ��� VARCHAR(40)
	  , ������ MONEY
	  , ���������� DATE
	  , ������������� DATE
	  , �������� VARCHAR(40)
	)
BEGIN
	DECLARE @name VARCHAR(50), @projId INT, @kol INT
	DECLARE curs CURSOR FOR
		SELECT ��� FROM �������
	OPEN curs
	FETCH curs INTO @projId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @kol = COUNT(*)
				FROM ����������, [���� � �������]
				WHERE [���� � �������].[��� �������] = @projId
				AND [��� ����������] = ����������.���
		if (@kol = @kolvo)
		BEGIN
			INSERT INTO @res 
				SELECT �������.���, ���, ������, [���� ������], [���� ���������], ���������.������������
				FROM ������� LEFT JOIN ��������� ON ���������.��� = [��� ���������]
				WHERE �������.��� = @projId
		END
		FETCH curs INTO @projId
	END
    RETURN
END
GO
SELECT * FROM dbo.�������������������(2)

-- ������ �� ���������� �����


-- ������� ��������� ���� ������ ������
DROP FUNCTION AvBall
GO
CREATE FUNCTION AvBall(@traineerId INT)
RETURNS INT
BEGIN
	DECLARE @answer INT = (
		SELECT AVG([��������� �����])
		FROM ����������
		WHERE [��� ������] = @traineerId
	)
	RETURN @answer
END
GO
SELECT ���, dbo.AvBall(���) AS [������� ����] FROM �������������

GO

-- ��������

-- �������� ��������
DROP TRIGGER trg�����D
GO
CREATE TRIGGER trg�����D ON [����� ��� ��������] AFTER DELETE
AS
DELETE FROM ���������� WHERE [��� �����] IN (SELECT ��� FROM deleted)
GO

-- ����������� �� ��������
DROP TRIGGER trg������D
GO
CREATE TRIGGER trg������D ON ������
AFTER DELETE
AS
If EXISTS (SELECT * FROM ��������� WHERE [��� ������] IN (SELECT ��� FROM deleted))
BEGIN
  RAISERROR('������ ������� ������, ��� ��� ���������� ���������!', 16, 2)
  ROLLBACK TRAN
END
GO

-- ����������� �� ����������
DROP TRIGGER trg����������IU
GO
CREATE TRIGGER trg����������IU ON ����������
AFTER INSERT, UPDATE
AS
IF EXISTS (SELECT * FROM inserted WHERE [��� ������] NOT IN (SELECT ��� FROM �������������))
BEGIN
	RAISERROR('������ �������� ������, ������ �� ����������', 16, 2)
	ROLLBACK
END
GO


-- ������ ���������� �� ��������� ������ � ��������� ����������
DROP PROC ������
GO
CREATE PROC ������ @country VARCHAR(40), @tech VARCHAR(40)
AS
  DECLARE @projId INT, @custId INT, @countryId INT, @techId INT

  SELECT @projId = MIN(���) FROM �������

  WHILE @projId IS NOT NULL
  BEGIN
    SELECT @custId = [��� ���������] FROM ������� WHERE ��� = @projId
    SELECT @countryId = [��� ������] FROM ��������� WHERE ��� = @custId
    SELECT @techId = [��� ����������]
      FROM [���������� � �������]
      WHERE [��� �������] = @projId

    IF (EXISTS (
        SELECT * FROM [���������� � �������]
        WHERE [��� �������] = @projId
          AND [��� ����������] IN (
            SELECT ��� 
            FROM [���� ����������]
            WHERE �������� = @tech
          )
      ) AND @country = (
        SELECT ��������
        FROM ������
        WHERE ��� = @countryId)
    )
    BEGIN
      UPDATE �������
      SET ������ = ������ * 0.90
      WHERE ��� = @projId
    END
    SELECT @projId = MIN(���) FROM ������� WHERE ��� > @projId
  END
GO

SELECT * FROM �������
BEGIN TRAN
EXEC ������ '��������', 'JavaScript'
SELECT * FROM �������
ROLLBACK TRAN

-- ��������������� ����� �� ����������

DROP PROC �����������������
GO
CREATE PROC ����������������� @tech VARCHAR(40)
AS
  DECLARE @techId INT
  SELECT @techId = ��� FROM [���� ����������] WHERE �������� = @tech
  DELETE FROM [����� ��� ��������]
  WHERE [��� ����������] = @techId
GO


SELECT * FROM [����� ��� ��������]
BEGIN TRAN
EXEC ����������������� 'C++'
SELECT * FROM [����� ��� ��������]
ROLLBACK TRAN